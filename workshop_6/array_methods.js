// mutbility
const name = "2ecinu";  // immutable
name[0] = "a"
const names = ["Luka", "Nika", "Mari", "Nuca", "Gio", "Mari"];  // array - მასივი

const age = 15;

// Methods
// push - adds new element to back
// names = 15;
console.log(names);
names.push("Joe");
console.log(names);
names.push("James");
console.log(names);
names.push("John");
console.log(names);

// pop - removes element from back
names.pop();
console.log(names);
names.pop();
console.log(names);


// shift
names.shift();
console.log(names);

// unshift
names.unshift("Gela");
console.log(names);

// indexOf
console.log(names.indexOf('Gio'));
console.log(names.indexOf('Mari'));

// lastIndexOf
console.log(names.lastIndexOf('Mari'));


// filter
function filterMari(name) {
    return name === 'Mari';
}
// higher functions
console.log(names.filter((juju) => juju.length === 3));
console.log(names.filter(filterMari));


// slices
console.log(names.slice(3, 6)); // Hard code
console.log(names.slice(names.length - 3, names.length)); // dynamic
console.log(names.slice(names.length - 3));
console.log(names.slice(-3));
console.log(names.slice(-3));
console.log(names.slice(-4, -1));

// concat
console.log(names.concat(["Lepard", "Tarzani", "Shifu"]));
console.log(names);


