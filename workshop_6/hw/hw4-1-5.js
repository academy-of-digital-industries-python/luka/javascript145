/**
 * დაწერეთ ფუნქცია, რომელიც "string" -ების "array" -ში დათვლის საშუალოდ რა ზომის "string" -ებია
 */
const strings = ['11234', 'rwefe', 'rtbrtb', '12', '1', '143', '1'];
let sum = 0;
for (const string of strings) {
    sum += string.length;
}

console.log(sum / strings.length);