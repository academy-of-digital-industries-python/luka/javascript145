/**
 * მოცემულია ორი "array", productNames და productPrices.
 * productNames -ში ჩაყრილია "string" -ები.
 * prdoctuPrices -ში ჩაყრილია "number" -ები.
 * ორივე "array"მ-ს ერთი და იგივე ზომა აქვთ!
 * დაწერეთ ფუნქცია, რომელიც მიიღებს 2 პარამეტრს, (productName, productPrice)
 * და დააბრუნებს HTML-ის კოდს "string" -ად შემდეგი სტრუქტურით:
 * 
 * <div>
 *      <p>Product Name</p>
 *      <p>Product Price$</p>
 * </div>
 * 
 * თითოეული ელემენტისთვის დააგენერირეთ ეს HTML კოდი და დაწერეთ "console.log" -ში მთლიანი აგებული სტრინგი.
 * ანუ ყველა პროდუქტის HTML -ი ერთ დიდ "string" -ში უნდა შეინახოთ!
 */


const productNames = ["banana", "strawberry", "ცherry"];
const productPrices = [8.99, 5.65, 6.5];

function productHTML(productName, productPrice) {
    return `
    <div>
        <p>${productName}</p>
        <p>${productPrice}$</p>
    </div>`;
}

let html = '';
for (let i = 0; i < productNames.length; i++){
    html += productHTML(productNames[i], productPrices[i]);
}

console.log(html);