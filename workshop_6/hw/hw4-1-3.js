/**
 * დაწერეთ ფუნქცია, რომელიც პარამეტრად მიიღებს "array" -ს.
 * ამ ფუნქციამ უნდა იპოვოს გადაცემულ "array" -ში რამდენი ისეთი "string" -ია, რომელიც შეიცავს
 * დიდ ასოს.
 * 
 * მაგალითი:
 * const someArray = ["String", "string", "StRing"];
 * 
 * შედეგი:
 * 2 (რადგან, ორი ისეთი "string" -ია, რომელიც დიდ ასოს შეიცავს)
 * 
 * კითხვა:
 * აქვს თუ არა თქვენ მიერ დაწერილი ფუნქციას side effect -ი?
 * 
 * 
 * 
 * ამოხსნა:
 * CAPITAL_LETTERS
 * let count be 0
 * let strings be "brb", "ererg", "AFFE"
 * 
 * for each string in strings:
 *      for each character in string:
 *              for each CAPITAL_LETTER in CAPITAL_LETTERS:
 *                  check if CAPITAL_LETTER equals to character:
 *                      
 *              if character is in CAPITAL_LETTERS:
 *                  // this string has capital letter
 *                  increase count by 1
 *                  go to the next string
 * 
 * 
 * 
 */

const strings = ["evtnv", "MNVUQ", "qRa"];
// const CAPITAL_LETTERS = ["A", "B", "C", "G", 'R', 'M', "U", 'V', 'Q', 'N'];
let count = 0;

for (let y = 0; y < strings.length; y++){
    const string = strings[y];

    for (let i = 0; i < string.length; i++) {
        const character = string[i];

        if (character.charCodeAt() >= 65 && character.charCodeAt() <= 90) {
            count++;
            break;
        }

        // for (let j = 0; j < CAPITAL_LETTERS.length; j++) {
        //     if (CAPITAL_LETTERS[j] === character) {
        //         console.log('this string has capital letter');
        //         count++;
        //         contains_capital = true;
        //         break;
        //     }
        // }
    }
}

console.log(strings[1.1])

console.log(count);
