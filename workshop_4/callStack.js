function g(x) {
    console.log('Calling g');
    returnValue = x * 2;
    console.log('Finished g');
    return returnValue;
}
function f(x) {
    console.log('Calling f');
    gRetrunValue = g(x);
    returnValue =  gRetrunValue + 1
    console.log('Finished f');
    return returnValue;
}

// const h = (x, y) => {
//     return x * y;
// };
const h = (x, y = 3) => f(y) * x; // ((y * 2) + 1) * x;

// console.log(f(3));
console.log(h(3)); 