"use strict";
// alert('Hi there I am alert');
// console.log(confirm('is it raining outside?'));
// console.log(Math.pow(2, 5));

const greet = function(name, age) {
    // definition
    // local scope
    const lastName = "test"; // shadowing
    console.log('Started executing greet');
    console.log(`hello ${name} ${age} years old ${lastName}`);
    console.log('finished executing greet');
    return `Bye ${name}`;
}
/*
greet(); // invoke / call
greet();
greet();
greet;
*/
const lastName = "Yochag";
const age = 25; // global scope
console.log(greet);
console.log(greet("Luka", age));
console.log(greet("Lazare", 27));


function saveToDB(username, password) {
    console.log('Saving to database');
    function checkPassword() {
        if (password.length < 8) {
            const s = 8;
            console.log('Password should be grater than ' + String(s));
            return false;
        }
        return true;
    }
    if (!checkPassword()) {
        return;
    }
   
    // console.log('s = ', s);
    console.log('Saving ', username, password, 'to database');
    console.log('Success');
    return password;
}

const password = saveToDB("Legend27", "12");
console.log(password);
// checkPassword();