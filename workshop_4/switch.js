const age = 27;

if (age === 25) {
    console.log('coool');
} else if (age === 26) {
    console.log('cool +1');
} else if (age === 27) {
    console.log('berdebi');
} else if (age === 28) {
    console.log('daberdi ukve');
} else if (age === 29) {
    console.log('29');
} else if (age === 30) {
    console.log('winaa yvelaferi');
}

switch (age) {
    case 25:
        console.log('coool');
        break;
    case 26:
        console.log('Cool + 1');
        break;
    case 27:
        console.log(27);
        break;
    case 28:
        console.log(28);
        break;
    case 29:
        console.log(29);
        break;
    case 30:
        console.log(30);
        break;
    default:
        console.log('?');
        break;
}