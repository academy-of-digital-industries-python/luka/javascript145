const API_BASE_URL = 'https://dog-api.kinduff.com/api';
const numberOfFacts = 5;


fetch(`${API_BASE_URL}/facts?number=${numberOfFacts}`)
    .then(response => response.json())
    .then(data => {
        data.facts.forEach(fact => {
            const div = document.createElement('div');
            const p = document.createElement('p');
            const img = document.createElement('img');
            img.width = 100;
            // img.src = 'https://random.dog/b1a59f58-452a-4d97-82bb-a70d75c33090.JPG';
            fetch('https://random.dog/woof.json')
                .then(response => response.json())
                .then(imgData => img.src = imgData.url);
            p.innerText = fact;
            div.appendChild(img);
            div.appendChild(p);
            document.body.appendChild(div);
        })
    })
    .catch(error => console.log(error))
    .finally(() => document.querySelector('#loading').style.display = 'none');
