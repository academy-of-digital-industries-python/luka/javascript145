const productForm = document.querySelector('#product-search-form');
const searchField = document.querySelector('#search-input');
const productList = document.querySelector('#products-list');

// How to get product
const createRandomProduct = (i) => {
    return {
        title: `Produdct ${i}`,
        price: Math.floor((Math.random() * 1000) + 300),
        render: function () {
            const div = document.createElement('div');
            div.classList.add('product-card');
            div.innerHTML = (`
                    <p>${this.title}</p>
                    <p>${this.price}</p>
                `);
            return div;
        }
    }
}

// ლოგიკა რომლითაც ბაზიდან მოგაქვს მონაცემები
const fetchProducts = new Promise((resolve, reject) => {
    // reject();
    setTimeout(() => {
        const products = [];

        for (let i = 0; i < 100; i++) {
            products.push(createRandomProduct(i));
        }
        resolve(products);
    }, 5000);
});

// Fetch DB
// const products = fetchProducts();
// რომ მოვა რა უქნა
let products = [];
fetchProducts
    .then(_products => {
        products = _products
        renderProducts(products);
    })
    .catch(() => {
        productList.innerText = "Something went wrong!";
    })
    .finally(() => {
        // stop loading
        document.querySelector('#loading').style.display = 'none';
    })

const filterProducts = () => {
    const searchValue = searchField.value;
    // filtering
    return products.filter(product => product.title.includes(searchValue));
}

const renderProducts = (products) => {
    console.log(products);
    productList.innerHTML = '';
    products
        .sort((p1, p2) => p1.price - p2.price)
        .forEach((product) => productList.appendChild(product.render()));
}
console.log(products);


// Filtering
productForm.onsubmit = (e) => {
    // Taking input from user
    e.stopPropagation();
    e.preventDefault();
    const filteredProducts = filterProducts();
    // Render
    console.log(filteredProducts);
    renderProducts(filteredProducts,);
}

searchField.addEventListener('input', e => {
    renderProducts(filterProducts());
})