const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        const success = Math.random();
        if (success > 0.5) {
            resolve(success);
        }
        reject(success);
    }, 2000);
});


promise
    .then((result) => {
        console.log('everything went fine', result);
    })
    .catch((errorCode) => {
        console.log('something went wrong', errorCode);
    })