const student = {
    firstName: "Joe",
    lastName: "Beiden",
    age: 24,
    goToUniversity: function () {
        console.log(this.firstName, this.lastName, 'Is going to the University!');
    }
}

class Student {
    constructor(name, lastName, age) {
        this.firstName = name;
        this.lastName = lastName;
        this.age = age;
    }

    goToUniversity() {
        console.log(this.firstName, this.lastName, 'Is going to the University!');
    }
};

const student1 = new Student("Jack", "Sparrow", 20);
const student2 = new Student("Leo", "Sparrow", 20);

student1.goToUniversity();
student2.goToUniversity();

