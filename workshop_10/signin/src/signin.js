/**
 * redirect user after submiting form correctly.
 * create users array and put random user, password pairs
 */

const signinForm = document.getElementById('signin-form');
const signingFormInputs = signinForm.querySelectorAll('input');
const [usernameInput, passwordInput] = signingFormInputs;
const users = [
    {
        username: "User2",
        password: "test"
    },
    {
        username: "User1",
        password: "test"
    }
];

function checkUser(__username, __password) {
    for (const {username, password} of users) {
        if (__username === username && __password === password) {
            return true; 
        }
    }
    return false;
}

signinForm.addEventListener('submit', event => {
    event.preventDefault();
    event.stopPropagation();
    console.log(usernameInput.value, passwordInput.value);
    if (checkUser(usernameInput.value, passwordInput.value)) {
        console.log('Success');
        // window.location.href = './index.html'
        window.location.assign('./index.html');
    }

    usernameInput.insertAdjacentHTML('afterend', '<p>Check username</p>');
});