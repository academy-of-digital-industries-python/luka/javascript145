/**
 * after clicking button show loading screen for 5 second and then redirect user to another HTML
 * use setTimeout 
 */
const loading = document.querySelector('#loading');
const content = document.querySelector('#content');

let dots = 0;
content.style.display = 'none';
setTimeout(() => {
    content.style.display = 'block';
    loading.style.display = 'none';

    const clock = document.createElement('div');
    const clock2 = document.createElement('div');

    clock.innerText = 'Your Local Time: ' + (new Date()).toLocaleString();
    clock2.innerText = 'Lodnon: ' + (new Date()).toLocaleString('en-GB', { timeZone: 'Europe/London' });

    setInterval(() => {
        clock.innerText = 'Your Local Time: ' + (new Date()).toLocaleString();
        clock2.innerText = 'Lodnon: ' + (new Date()).toLocaleString('en-GB', { timeZone: 'Europe/London' });
    }, 1000);
    content.appendChild(clock);
    content.appendChild(clock2);

    // content.innerHTML = '<p>legtnewuo</p>'.repeat(10);

    clearInterval(loadingInterval);
}, 5000);

const loadingInterval = setInterval(() => {
    if (dots > 3)
        dots = 0;
    loading.innerText = `loading${'.'.repeat(dots)}`;
    dots++;
}, 400);
