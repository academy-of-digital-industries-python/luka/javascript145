/**
 * Write form and get values on submit with javascript
 */

const userForm = document.querySelector('#user-form');
const firstNameInput = userForm.querySelector('#firstName-id');
const lastNameInput = userForm.querySelector('#lastName-id');

userForm.onsubmit = (event) => {
    event.stopPropagation();

    const pattern = /\d+/;
    if (pattern.test(firstNameInput.value) || pattern.test(lastNameInput.value)){
        event.preventDefault();
        console.log('Your form is not correct');
        firstNameInput.style.border = '1px solid red';
        firstNameInput.insertAdjacentHTML('afterend', `<p style="color: red">First name contains number!</p>`)
        lastNameInput.style.border = '1px solid red';
        lastNameInput.insertAdjacentHTML('afterend', `<p style="color: red">Last name contains number!</p>`)

        return;
    }

    console.log(`${firstNameInput.value} and ${lastNameInput.value} saved!`);
};
// userForm.addEventListener('submit')