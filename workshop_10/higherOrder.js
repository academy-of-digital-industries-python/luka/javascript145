const nums = [1, 2, 3, 4, 5];

function getSum(accumulator, curr) {
    return accumulator + curr;
}
console.log(
    nums.reduce(getSum)
);

function reduce(array, callbackFn) {
    let sum;
    for (const element of array) {
        sum = callbackFn(sum, element)
    }
    return sum;
}

console.log(reduce(nums, getSum));
// console.log(reduce(nums, (newArr, element) => {
//     if (!(newArr instanceof Array)) {
//         return [element];
//     }
//     return [...newArr, element];
// }));
