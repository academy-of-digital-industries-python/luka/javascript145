const couple = ["Josh", "Jane"];


console.log('\n=== Index ===');
console.log(couple[0], couple[1]);

console.log('\n=== In varaibles ===');
const groom = couple[0];
const bride = couple[1];
console.log(groom, bride);

const [_groom, _bride] = couple;

console.log('\n=== Destructure ===');
console.log(_groom, _bride);

const user = {
    firstName: "Khvicha",
    lastName: "Kvara",
    age: 22
};

console.log('\n=== Object Destructure ===');
const {firstName, lastName} = user;
console.log(firstName, lastName);