const cars = [
    {
        name: "Tesla Model X",
        price: 45000
    },
    {
        name: "Tesla Model Y",
        price: 46000
    },
    {
        name: "BMW X5",
        price: 30000
    },
    {
        name: "Toyta Raptor",
        price: 90000
    }
];
cars.forEach(car => console.log(`${car.name} costs ${car.price}`));

// AVG Price
console.log(
    (
        cars.map(car => car.price)
            .reduce((prevPrice, currentPrice) => prevPrice + currentPrice)
    ) / cars.length
);

console.log(
    cars.map(car => car.name)
        .reduce((prevCar, currentCar) => prevCar + currentCar)
)

console.log(
    cars.filter(car => car.price < 46000)
)
