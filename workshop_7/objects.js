/**
 * Data Structure: Object
 * key: value - გასაღები: მნიშვნელობა
 */

// Instantiation / creation
const student = {
    "firstName": "Joe",
    'lastName': "Doe",
    age: 45,
};

// access - key
const key = 'firstName'
Object.assign(student, {
    family: 7,
    car: 'Tesla Model Y'
});
console.log(student.age);
console.log(student[key]);

// modify
student.age = 19;
student.friends = ["Josh", " jane", "kvicha", "gocha"];
console.log(student);

// create new key-value pair
student.gpa = 4;
console.log(student);
// console.log(student.farther);

// delete key-value pair
delete student.gpa;
console.log(student);
console.log(student.friends);

console.log(typeof student);
console.log('\n=== Keys ===');
for (const gela of Object.keys(student)) {
    console.log(gela, student[gela]);
}

console.log('\n=== Values ===');
for (const value of Object.values(student)) {
    console.log(value);
}
console.log('\n=== Entires ===');
for (const both of Object.entries(student)) {
    console.log(both[0], both[1]);
}

console.log('\n=== Entires (Destructure) ===');
for (const [key, value] of Object.entries(student)) {
    console.log(key, value);
}
