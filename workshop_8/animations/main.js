const rainDrops = [];
const rainWrapper = document.querySelector('#rain-wrapper');

function getRainDrop() {
    const rainDrop = document.createElement('div');
    rainDrop.classList.add('rain-drop');
    return rainDrop;
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max + 1);
}

for (let i = 0; i < 100; i++){
    rainDrops.push(getRainDrop());
}


for (const rainDrop of rainDrops) {
    rainDrop.style.left = `${getRandomInt(100)}%`;
    rainWrapper.appendChild(rainDrop);
}

setInterval(() => {
    for (const rainDrop of rainDrops) {
        // console.log(rainDrop, rainDrop.style.top);
        let top = parseInt(rainDrop.style.top);
        if (!top || top > rainWrapper.clientHeight) {
            top = getRandomInt(-400);
            rainDrop.style.left = `${getRandomInt(100)}%`;
        }
        rainDrop.style.top = `${top + 10}px`;
    }
}, 10);
