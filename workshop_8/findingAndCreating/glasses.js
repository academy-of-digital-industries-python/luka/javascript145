const contentDiv = document.getElementsByTagName('div')[0];
console.log(contentDiv.firstChild.previousSibling);
// const generatorButton = document.getElementById('glass-generator');
const generatorButton = document.querySelector('#glass-generator');
const output = document.getElementById('output');

// addEventListener('click', () => {
//     console.log(Math.random() * 5);
// });
generatorButton.onclick = () => {
    output.innerText = Math.random() * 5;
}

console.log(generatorButton);
// console.log(document.getElementsByClassName('btn btn-danger'));

const replaceImages = () => {
    const imgs = Array.from(document.querySelectorAll('img'));
    imgs[0].remove();
    // imgs[1].parentNode.appendChild(imgs[0]);
    // const newImage = document.createElement('img');
    // newImage.src = imgs[0].src;
    // newImage.alt = "new Image";
    // newImage.width = 50;
    imgs[1].parentNode.insertBefore(imgs[0], imgs[1]);
    imgs.forEach((img) => {
        console.log(img.getAttribute('src'));
        console.log(img.alt);

        img.parentNode.replaceChild(document.createTextNode(img.alt), img);
    });
    // console.log(imgs);
    // for (const img of imgs) {
    //     console.log(img);
    // }
}