let age = 5;

// age = age + 7;
age += 7; // 12
age /= 2; // 6
age *= 3; // 18
age++; // 19
++age; // 20
--age; // 19
age--; // 18
// console.log(--age);
console.log(age--);
