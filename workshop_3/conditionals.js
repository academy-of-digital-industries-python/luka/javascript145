const firstName = prompt("Enter your name: ");
const age = Number(prompt("Enter your age: "));

// const is_adult = age >= 18;
// {
//     const lastName = "L";
// }
// console.log(lastName);

if (age >= 18) { // 18 <= age
    // code block; fragment 
    console.log(`${firstName} ${age} years old; Adult!`);
    console.log("Welcome");
    console.log("Hello");
} else if (age === 15) {
    console.log('Take candy');
} else {
    console.log(`${firstName} ${age} years old; Minor!`);
}


if (firstName === 'Gochrevan') {
    console.log('Take 1000$');
} else {
    console.log('Give me 5$');
}
