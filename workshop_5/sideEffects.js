let n = 7;


function multiply(n) {
    return n * 2;
}

function multiplyWithSideEffect() {
    n *= 2; // n = n * 2;
    // return n * 2;
}

function multiplyWithoutSideEffect() {
    return n * 2;
}

console.log(multiply(5));
multiplyWithSideEffect(); // 14
console.log(n);
multiplyWithSideEffect(); // 28
multiplyWithSideEffect(); // 56
multiplyWithSideEffect(); // 112
console.log(n);