/*
Data Structre: Array
*/
// const oddNumbers = "1, 3, 5, 7, 9";
const numbers = [5, 6, 4, 5, 6, 4, 4, 6, 6, 4, 6];
const names = ["Gio", "Luka", "Nka"];

// console.log(numbers[0]);
// console.log(numbers[1]);
// console.log(numbers.length);
// console.log(numbers[7]);
// console.log(numbers[numbers.length - 1]);
// console.log(numbers);

// console.log(numbers[0]);
// console.log(numbers[1]);
// console.log(numbers[2]);
// console.log(numbers[3]);
// console.log(numbers[4]);
// console.log(numbers[5]);
// console.log(numbers[6]);
// console.log(numbers[7]);

let sum = 0;

// sum = sum of all elements in this list
for (let i = 0; i < numbers.length; i++){
    sum += numbers[i];
    // console.log(numbers[i]);
}

console.log(`SUM of this list numbers is ${sum}`);
console.log(`AVG in this list is ${sum / numbers.length}`);

console.log('Length: ', numbers.length);
sum = 0;
for (const i in numbers) {
    sum += numbers[i];
}
console.log(sum);

sum = 0;
for (const number of numbers) {
    // console.log(number);
    sum += number;
}

console.log(sum)