function wrapper() {
    // Local Scope
    let __name = "Gio";
    console.log('From f function', __name);

    return () => {
        console.log('From anonnymus function', __name);
    }
}

function productCard(title, price, options) {
    return `
    <div>
        <p>${title}</p>
        <small>${price}</small>
    </div>
    `
};

console.log(productCard());


function mount() {
    console.log('ჩასმის ეტაპი');
    return () => {
        console.log('მოხსნის ეტაპი')
    }
}


const result = wrapper();
result();

