const div = document.querySelector('div');
const button = document.querySelector('button');
console.log(typeof button);
console.log(button);
const mousDiv = document.createElement('div');

mousDiv.style.width = `50px`;
mousDiv.style.height = `50px`;
mousDiv.style.borderRadius = `50%`;
mousDiv.style.borderTopLeftRadius = `0`;
mousDiv.style.backgroundColor = `blue`;
mousDiv.style.position = 'absolute';
// document.body.appendChild(mousDiv);



// addEventListener('mouseover', () => console.log('Hello'));
div.addEventListener('mousedown', () => console.log('Handler for grand div'));
// console.log(div.childNodes);
div.childNodes[1].addEventListener('mousedown', (event) => {
    event.stopPropagation();
    console.log('Handler for p');
});

button.addEventListener('mousedown', (event) => {
    event.stopPropagation();
    console.log(event.target.nodeName);
    console.log('I was clicked!');
});


document.querySelector('a').addEventListener('click', event => {
    event.preventDefault();

    setTimeout(() => {
        window.location.href = event.target.href;   
    }, 4000);
});

let target = 0;
let clock = setInterval(() => {
    console.log(target)
    if (target == 10){
        clearInterval(clock);
    }
    target++;
}, 1000);

document.querySelector('input').addEventListener('change', (e) => console.log(e.target.value));

// document.addEventListener('keydown', event => {
//     console.log(event.key);
// })

window.addEventListener('mousemove', event => {
    // console.log(event.pageX, event.pageY);
    mousDiv.style.left = `${event.pageX - 25}px`;
    mousDiv.style.top = `${event.pageY - 25}px`;
})
