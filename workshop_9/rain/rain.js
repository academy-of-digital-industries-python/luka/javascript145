function create_rain_drops(amount) {
    for (let i = 0; i < amount; i++) {
        const rainDrop = document.createElement('div');
        const size = Math.random() * 3;
        const posX = Math.floor(Math.random() * window.innerWidth);

        rainDrop.classList.add('rain-drop');
        rainDrop.style.width = `${size}px`;
        rainDrop.style.top = `${-200}px`;
        rainDrop.style.left = `${posX}px`;
        rainDrop.style.animationDelay = (Math.random() * -20) + 's';
        rainDrop.style.animationDuration = 2 + (Math.random() * 5) + 's';

        document.body.appendChild(rainDrop);
    }
}

create_rain_drops(500);