import config from "../config.json" assert {type: "json"};

const title = document.querySelector('h1');
const img = document.querySelector('img');
const desc = document.querySelector('p');

function get(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
}

const productURL = `${config.apiURL}/products/${get('id')}`;

const fetchProduct = async (prodcutURL) => {
    const response = await fetch(prodcutURL);
    const data = await response.json();
    return data;
}

const renderProduct = async () => {
    const product = await fetchProduct(productURL);
    title.innerText = product.title;
    desc.innerText = product.description;
    img.src = product.thumbnail;
    console.log(product)
}
renderProduct();