import { signAuthToken } from "./auth.js";
import routes from "../routes.json" assert { type: "json"};

if (!signAuthToken(sessionStorage.getItem('token'))) {
    window.location.href = routes.signIn;
}