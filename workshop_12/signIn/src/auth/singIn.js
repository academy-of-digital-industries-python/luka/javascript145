import { TEST_VAR, signAuthToken, authenticateUser } from "./auth.js";
import routes from "../routes.json" assert { type: "json" };

const signInForm = document.querySelector('#sign-in-form');
const userInput = signInForm.querySelector('#usernameInputId');
const passInput = signInForm.querySelector('#passwordInputId');
const errorMessagesDiv = signInForm.querySelectorAll('div')[2];

// localStorage.setItem('Test', 'hello');
// sessionStorage.setItem('test', 'hello');
if (signAuthToken(sessionStorage.getItem('token'))) {
    window.location.href = routes.home;
}

signInForm.addEventListener('submit', event => {
    event.stopPropagation();
    event.preventDefault();
    try {
        const token = authenticateUser(userInput.value, passInput.value);
        window.location.href = routes.home;
        sessionStorage.setItem('token', token);
    } catch {
        errorMessagesDiv.innerText = "Check Username or Password";
    }
})
