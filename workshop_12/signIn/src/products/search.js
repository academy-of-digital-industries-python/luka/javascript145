const searchProducts = (products, title) => products.filter(product => product.title.toLowerCase().includes(title.toLowerCase()));

export default searchProducts;