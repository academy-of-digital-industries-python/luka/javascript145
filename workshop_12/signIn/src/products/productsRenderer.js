import { fetchProducts } from "./fecthProducts.js";
import productCard from "./productCard.js";
import searchProducts from "./search.js";

const productsContainerDiv = document.querySelector('#productsContainer');
const searchForm = document.querySelector('#searchForm');
const searchField = document.querySelector('#searchField');
const suggestionsDiv = document.querySelector('#suggestions-div');

let allProducts = [];
document.body.onclick = (event) => {
    if (event.target.id === "suggestions-div") {
        return;
    }
    suggestionsDiv.style.display = 'none';
}
const getProductsHTML = (products) => products
    .map(product => productCard(product))
    .join('\n');


const renderProducts = (products) => {
    productsContainerDiv.innerHTML = '';
    if (products.length === 0) {
        productsContainerDiv.innerHTML = '<p>No macthing query</p>';
    } else {
        productsContainerDiv.innerHTML = getProductsHTML(products);
    }
};

const fetchAndRenderProducts = async () => {
    const { products } = await fetchProducts();
    console.log(products[0]);
    allProducts = products;
    renderProducts(allProducts);
};

searchForm.onsubmit = (event) => {
    suggestionsDiv.style.display = 'none';
    event.stopPropagation();
    event.preventDefault();

    renderProducts(searchProducts(allProducts, searchField.value));
}

const selectProduct = (title) => {
    // window.location.href = "someProduct.html";
    searchField.value = title;
    suggestionsDiv.style.display = 'none';
    renderProducts(searchProducts(allProducts, title));
}

searchField.addEventListener('input', event => {
    if (!event.target.value) {
        suggestionsDiv.style.display = 'none';
        return;
    }
    suggestionsDiv.innerHTML = "";
    suggestionsDiv.style.display = 'block';
    searchProducts(allProducts, event.target.value)
        // .slice(0, 5)
        .forEach(({ thumbnail, title }) => {
            const p = document.createElement('p');
            p.innerHTML = (`<img width=30 height=30 src="${thumbnail}"/> ${title}`);
            p.onclick = () => selectProduct(title);
            suggestionsDiv.appendChild(p);
        })

    renderProducts(searchProducts(allProducts, event.target.value));
})

fetchAndRenderProducts();