export const productCard = (product) => {
    return (`<div class="col" id="${product.id}">
        <div class="card w-100">
            <img width="286" height="200" style="object-fit: cover;" src="${product.thumbnail}" class="card-img-top" alt="${product.title}">
            <div class="card-body">
                <h5 class="card-title">${product.title}</h5>
                <div class="badge bg-info">${product.category}</div>
                <p class="card-text">${product.description.substring(0, 60)}</p>
                <a href="./product.html?id=${product.id}" class="btn btn-primary">Show more</a>
            </div>
        </div>
    </div>`);
};

export default productCard;