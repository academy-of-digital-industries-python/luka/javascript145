import config from '../config.json' assert { type: "json" };

console.log(config);

export const fetchProducts = async () => {
    const response = await fetch(`${config.apiURL}/products/`);
    const products = await response.json();
    return products;
};