'use strict';
// var firstName = "John";
// var age = 45;
let firstName = "Joe"; // declaration
let age = 45;
const LAST_NAME = "Doe"; // constant variable

console.log(firstName, age);
// LAST_NAME = "Gia";

age = 67;
// console.log('Hello ' + firstName);
console.log(`Hello ${firstName} ${LAST_NAME} I'm ${age} years old`);
