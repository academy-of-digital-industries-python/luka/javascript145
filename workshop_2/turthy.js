console.log('!0 ==', !0); // true
console.log('!1 ==', !1); // false
console.log('!1000 ==',!1000); // false
console.log('!-459 ==',!-459); // false
console.log("!\"Hello\" ==", !"Hello"); // false
console.log('!!"Hello" ==', !!"Hello"); // true
console.log('!"" ==', !"");
console.log('!!"" ==', !!"");

