// let firstName = prompt("Enter your name: ");
// console.log(firstName);
// Type casting
let num1 = Number(prompt('Enter a number:'));
let num2 = Number(prompt('Enter a number:'));

console.log(num1 + num2);
console.log(typeof num1); // Number
console.log(String(num1) + String(num2));
// num1 = String(num1);
console.log(typeof num1); // String
console.log(!!num1, Boolean(num1));
console.log(Math.max(num1, num2));
console.log(Math.min(num2, num1));
