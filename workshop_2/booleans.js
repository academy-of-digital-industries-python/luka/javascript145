console.log(true, false);

// comparison operators
console.log('9 == 8', 9 == 8);
console.log('10 == 10', 10 == 10);
console.log('11 != 11', 11 != 11);
console.log('11 != 10', 11 != 10);
console.log('11 > 17', 11 > 17);
console.log('11 >= 11', 11 >= 11);
console.log('11 <= 7', 11 <= 7);
console.log('11 < 7', 11 < 7);
console.log('11 <= 12', 11 <= 12);

console.log("'John' == 'James'", 'John' == 'James');
console.log("'John' != 'James'", 'John' != 'James');
console.log("'John' > 'James'", 'John' > 'James');
console.log("'John' < 'James'", 'John' < 'James');

// Logical operators
console.log('&& Truth table');
console.log('true && true', true && true);
console.log('true && false', true && false);
console.log('false && true', false && true);
console.log('false && false', false && false);

console.log('|| Truth table');
console.log('true || true', true || true); // true
console.log('true || false', true || false); // true
console.log('false || true', false || true); // true
console.log('false || false', false || false); // false


console.log('! True table');
console.log('!true', !true);
console.log('!false', !false);


console.log('5 + 1 <= 8 && 7 >= 9', 5 + 1 <= 8 && 7 >= 9);
console.log('James' == 'Nick' && (9 < 90 || !false)); // false
console.log(!false && !true && true);

